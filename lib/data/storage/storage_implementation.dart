import 'dart:convert';
import 'dart:developer';

import 'package:flutter_nanolike_sdk/domain/models/filters.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_nanolike_sdk/domain/models/session.dart';
import 'package:flutter_nanolike_sdk/domain/models/token.dart';
import 'package:flutter_nanolike_sdk/domain/providers/storage_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StorageProviderImplementation implements IStorageProvider {
  final _secureStorage = const FlutterSecureStorage();
  static const _sharedPrefSession = 'session_key';
  static const _sharedPrefFilters = 'filters_key';
  static const _secureStorageToken = 'jwt_token_key';

  @override
  Future saveSession(Session session) async {
    final sessionJsonString = jsonEncode(session);
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(_sharedPrefSession, sessionJsonString);
  }

  @override
  Future<Session?> getSession() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(_sharedPrefSession)) {
      final sessionJsonString = prefs.getString(_sharedPrefSession);
      if (sessionJsonString != null) {
        final sessionJson = jsonDecode(sessionJsonString);
        return Session.fromJson(sessionJson);
      }
    }
    return null;
  }

  @override
  Future removeSession() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove(_sharedPrefSession);
  }

  @override
  Future<JWTToken?> getJWTToken() async {
    bool hasToken = false;

    try {
      hasToken = await _secureStorage.containsKey(key: _secureStorageToken);
    } catch (e) {
      log("getJWTToken: _secureStorage.containsKey exception: $e");
      return null;
    }

    if (hasToken) {
      String? tokenJsonString;

      try {
        tokenJsonString = await _secureStorage.read(key: _secureStorageToken);
      } catch (e) {
        log("getJWTToken: _secureStorage.read exception: $e");
        return null;
      }

      if (tokenJsonString != null) {
        final tokenJson = jsonDecode(tokenJsonString);
        return JWTToken.fromJson(tokenJson);
      }
    }
    return null;
  }

  @override
  Future saveJWTToken(JWTToken token) async {
    final tokenJsonString = jsonEncode(token);
    try {
      await _secureStorage.write(
          key: _secureStorageToken, value: tokenJsonString);
    } catch (e) {
      log("saveJWTToken: _secureStorage.write exception: $e");
    }
  }

  @override
  Future removeAll() async {
    try {
      await _secureStorage.delete(key: _secureStorageToken);
    } catch (e) {
      log("removeAll: _secureStorage.delete exception: $e");
    }

    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(_sharedPrefFilters)) prefs.remove(_sharedPrefFilters);
    if (prefs.containsKey(_sharedPrefSession)) prefs.remove(_sharedPrefSession);
  }

  @override
  Future<Filters?> getFilters() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(_sharedPrefFilters)) {
      final filtersJsonString = prefs.getString(_sharedPrefFilters);
      // FIXME: weird bug, if filtersJsonString is null, if condition pass
      if (filtersJsonString != null && filtersJsonString.isNotEmpty) {
        final filtersJson = jsonDecode(filtersJsonString);
        if (filtersJson != null) {
          return Filters.fromJson(filtersJson);
        }
      }
    }
    return null;
  }

  @override
  Future saveFilters({Filters? filters}) async {
    final filtersJsonString = jsonEncode(filters);
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(_sharedPrefFilters, filtersJsonString);
  }
}
