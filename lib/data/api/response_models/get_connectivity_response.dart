import 'package:flutter_nanolike_sdk/domain/models/connectivity_message.dart';

class ConnectivityResponse {
  final List<ConnectivityMessage> messages;

  ConnectivityResponse(this.messages);

  factory ConnectivityResponse.parse(List<dynamic> list) {
    final List<ConnectivityMessage> items =
        list.map((e) => ConnectivityMessage.fromJson(e)).toList();
    return ConnectivityResponse(items);
  }
}
