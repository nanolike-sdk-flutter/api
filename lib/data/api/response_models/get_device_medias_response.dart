import 'package:flutter_nanolike_sdk/domain/models/media.dart';

class GetDeviceMediasResponse {
  final List<Media> medias;

  GetDeviceMediasResponse(this.medias);

  factory GetDeviceMediasResponse.parse(List<dynamic> list) {
    final List<Media> medias = list.map((e) => Media.fromJson(e)).toList();
    return GetDeviceMediasResponse(medias);
  }
}
