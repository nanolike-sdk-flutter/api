import 'package:flutter_nanolike_sdk/domain/models/accessory.dart';

class GetGroupAccessoriesResponse {
  final List<Accessory> items;

  GetGroupAccessoriesResponse(this.items);

  factory GetGroupAccessoriesResponse.parse(List<dynamic> list) {
    final List<Accessory> items =
        list.map((e) => Accessory.fromJson(e)).toList();
    return GetGroupAccessoriesResponse(items);
  }
}
