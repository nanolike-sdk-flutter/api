import 'package:flutter_nanolike_sdk/domain/models/role.dart';

class GetWorkspaceRolesResponse {
  final List<Role> items;

  GetWorkspaceRolesResponse(this.items);

  factory GetWorkspaceRolesResponse.parse(List<dynamic> list) {
    final List<Role> items =
        list.map((e) => Role.fromWorkspaceJson(e)).toList();
    return GetWorkspaceRolesResponse(items);
  }
}
