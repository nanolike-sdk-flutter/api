import 'package:flutter_nanolike_sdk/domain/models/token.dart';

class JWTTokenResponse {
  final JWTToken jwtToken;

  JWTTokenResponse(this.jwtToken);

  factory JWTTokenResponse.parse(Map<String, dynamic> json) {
    final jwtToken = JWTToken(
      json['access'] as String,
      json['refresh'] as String,
    );

    return JWTTokenResponse(jwtToken);
  }
}
