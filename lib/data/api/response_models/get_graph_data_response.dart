import 'package:flutter_nanolike_sdk/domain/models/graph_data.dart';

class GetGraphDataResponse {
  final List<GraphData> data;

  GetGraphDataResponse(this.data);

  factory GetGraphDataResponse.parse(List<dynamic> list) {
    final List<GraphData> items =
        list.map((e) => GraphData.fromJson(e)).toList();
    return GetGraphDataResponse(items);
  }
}
