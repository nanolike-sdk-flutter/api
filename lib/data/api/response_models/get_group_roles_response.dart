import 'package:flutter_nanolike_sdk/domain/models/role.dart';

class GetGroupRolesResponse {
  final List<Role> items;

  GetGroupRolesResponse(this.items);

  factory GetGroupRolesResponse.parse(List<dynamic> list) {
    final List<Role> items = list.map((e) => Role.fromGroupJson(e)).toList();
    return GetGroupRolesResponse(items);
  }
}
