import 'package:flutter_nanolike_sdk/domain/models/device.dart';

class GetDevicesResponse {
  final List<Device> items;

  GetDevicesResponse(this.items);

  factory GetDevicesResponse.parse(List<dynamic> list) {
    final List<Device> items =
        list.map((e) => Device.fromDevicesJson(e)).toList();
    return GetDevicesResponse(items);
  }
}
