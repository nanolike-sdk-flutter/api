import 'package:flutter_nanolike_sdk/domain/models/order.dart';

class GetOrdersResponse {
  final List<Order> orders;

  GetOrdersResponse(this.orders);

  factory GetOrdersResponse.fromJson(Map<String, dynamic> json) {
    final List<dynamic>? results = json["results"];
    final List<Order> items = [];
    if (results != null) {
      items.addAll(results.map((e) => Order.fromJson(e)).toList());
    }
    return GetOrdersResponse(items);
  }
}
