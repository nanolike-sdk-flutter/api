class InviteResponse {
  final bool success;
  final bool userAlreadyExists;

  InviteResponse(this.success, this.userAlreadyExists);
}
