import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';

class GetSupplementsPaginatedRequest extends RequestPaginated {
  GetSupplementsPaginatedRequest({super.queryParam});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => 'assets/mocks/supplements_paginated.json';

  @override
  String get path => '/v2/supplements';

  @override
  Map<String, dynamic>? get header => null;
}
