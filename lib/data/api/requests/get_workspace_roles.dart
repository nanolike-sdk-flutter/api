import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetWorkspaceRolesRequest extends Request {
  GetWorkspaceRolesRequest();

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/workspace_acl_roles.json';

  @override
  String get path => '/v1/workspace-acl-roles/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
