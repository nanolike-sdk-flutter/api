import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class RefreshTokenRequest extends Request {
  final String _refreshToken;

  RefreshTokenRequest(this._refreshToken);

  @override
  AuthorizationType get authorizationType => AuthorizationType.basic;

  @override
  get data => jsonBody(_refreshToken);

  @override
  String get method => 'POST';

  @override
  String get path => '/v1/token/refresh/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;

  @override
  String get mock => 'assets/mocks/refresh.json';
}

Map<String, dynamic> jsonBody(String refreshToken) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['refresh'] = refreshToken;
  return data;
}
