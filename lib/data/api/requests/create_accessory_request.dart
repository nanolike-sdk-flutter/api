import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/domain/models/accessory.dart';

class CreateAccessoryRequest extends Request {
  final String _groupId;
  final String _accessoryId;
  final AccessoryType _accessoryType;

  CreateAccessoryRequest(this._groupId, this._accessoryId, this._accessoryType);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => jsonBody(_accessoryId);

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/post_accessory.json';

  @override
  String get path => '/v1/accessories/$_groupId/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;

  List<Map<String, dynamic>> jsonBody(String accessoryId) {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['accessory_id'] = accessoryId;
    data['type'] = _accessoryType.serverString();
    return [data];
  }
}
