import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetGraphDataRequest extends Request {
  final List<String>? deviceIds;
  final List<String>? groupIds;
  final List<String> dataTypes;
  final bool isLastValue;
  final DateTime? from;
  final DateTime? to;

  GetGraphDataRequest(
      {this.deviceIds,
      this.groupIds,
      required this.dataTypes,
      required this.isLastValue,
      this.from,
      this.to});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (deviceIds != null) {
      data["device_ids"] = deviceIds;
    }
    if (groupIds != null) {
      data["group_ids"] = groupIds;
    }
    if (from != null) {
      data["from_timestamp"] = from?.toIso8601String();
    }
    if (to != null) {
      data["to_timestamp"] = to?.toIso8601String();
    }
    data["data_types"] = dataTypes;
    data["is_last_value"] = isLastValue;
    return data;
  }

  @override
  String get method => 'POST';

  @override
  String get mock => "assets/mocks/graph_data.json";

  @override
  String get path => '/v1/get-graph-data/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
