import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';

class GetUsersPaginatedRequest extends RequestPaginated {
  GetUsersPaginatedRequest({super.queryParam});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => 'assets/mocks/users_paginated.json';

  @override
  String get path => '/v1/users';

  @override
  Map<String, dynamic>? get header => null;
}
