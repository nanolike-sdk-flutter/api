import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class ReinviteRequest extends Request {
  final String userId;

  ReinviteRequest({
    required this.userId,
  });

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/user.json';

  @override
  String get path => '/v1/users/$userId/reinvite';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
