import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class LoginRequest extends Request {
  final String _username;
  final String _password;
  final String? _workspace;

  LoginRequest(this._username, this._password, this._workspace);

  @override
  AuthorizationType get authorizationType => AuthorizationType.basic;

  @override
  bool get needWorkspaceHeader => false;

  @override
  get data => jsonBody(_username, _password, _workspace);

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/login.json';

  @override
  String get path => '/v1/token';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}

Map<String, dynamic> jsonBody(
    String username, String password, String? workspace) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['username'] = username;
  data['password'] = password;
  if (workspace != null) {
    data['workspace'] = workspace;
  }
  return data;
}
