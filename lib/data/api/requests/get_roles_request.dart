import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetMeRolesRequest extends Request {
  GetMeRolesRequest();

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/roles.json';

  @override
  String get path => '/v2/me/roles';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
