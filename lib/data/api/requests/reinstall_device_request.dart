import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_operation_type.dart';

class ReinstallDeviceRequest extends Request {
  final String deviceToMaintainReference;
  final String newDeviceReference;
  final DeviceOperationType deviceOperationType;

  ReinstallDeviceRequest(
      {required this.deviceToMaintainReference,
      required this.newDeviceReference,
      required this.deviceOperationType});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => {
        'idDevice': newDeviceReference,
        'clearHistory': false,
        'operationType': deviceOperationType.serverString
      };

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/empty.json';

  @override
  String get path => '/v2/devices/$deviceToMaintainReference/reinstall';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
