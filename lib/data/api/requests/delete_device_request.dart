import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class DeleteDeviceRequest extends Request {
  final String _deviceId;

  DeleteDeviceRequest(this._deviceId);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'DELETE';

  @override
  String get mock => "assets/mocks/empty.json";

  @override
  String get path => '/v1/devices/$_deviceId/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
