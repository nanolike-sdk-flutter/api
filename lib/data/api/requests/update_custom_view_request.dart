import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/domain/models/filters.dart';

class UpdateCustomViewRequest extends Request {
  final String customViewId;
  final String name;
  final Filters filters;

  UpdateCustomViewRequest(
      {required this.customViewId, required this.name, required this.filters});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => {'name': name, 'filters': filters.toJsonFromCustomView()};

  @override
  String get method => 'PATCH';

  @override
  String get mock => 'assets/mocks/custom_view.json';

  @override
  String get path => '/v2/custom-views/$customViewId';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
