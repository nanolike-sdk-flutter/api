import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetMeRequest extends Request {
  GetMeRequest();

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/me_workspace.json';

  @override
  String get path => '/v2/me';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
