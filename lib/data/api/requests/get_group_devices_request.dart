import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetGroupDevicesRequest extends Request {
  final String groupId;

  GetGroupDevicesRequest(this.groupId);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/group.json';

  @override
  String get path => '/v1/groups/$groupId/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
