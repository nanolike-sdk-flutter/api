import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetUserRequest extends Request {
  final String userId;

  GetUserRequest(this.userId);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/user.json';

  @override
  String get path => '/v1/users/$userId';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
