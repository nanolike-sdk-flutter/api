import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetDeviceRequest extends Request {
  final String _deviceId;

  GetDeviceRequest(this._deviceId);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => "assets/mocks/device.json";

  @override
  String get path => '/v1/devices/$_deviceId/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
