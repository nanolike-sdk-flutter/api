import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetWorkspaceSettingsRequest extends Request {
  GetWorkspaceSettingsRequest();

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/workspace_settings.json';

  @override
  String get path => '/v1/workspace/settings';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
