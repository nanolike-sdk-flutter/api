import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetRegisterRequest extends Request {
  final String token;

  GetRegisterRequest({required this.token});

  @override
  AuthorizationType get authorizationType => AuthorizationType.basic;

  @override
  bool get needWorkspaceHeader => false;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/empty.json';

  @override
  String get path => '/v1/register';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => {"token": token};
}
