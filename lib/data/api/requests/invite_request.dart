import 'dart:ui';

import 'package:flutter_nanolike_sdk/domain/models/role.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class InviteRequest extends Request {
  final String email;
  final String? firstName;
  final String? lastName;
  final List<String> groupIds;
  final Role role;
  final Locale language;

  InviteRequest(
      {required this.email,
      this.firstName,
      this.lastName,
      this.groupIds = const [],
      required this.role,
      required this.language});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => jsonBody(email, groupIds, role);

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/user.json';

  @override
  String get path => '/v1/invite/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;

  Map<String, dynamic> jsonBody(
      String email, List<String> groupIds, Role role) {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['group_ids'] = groupIds;
    data['idRole'] = role.id;
    data['lang'] = language.toLanguageTag();

    if (firstName != null) data['first_name'] = firstName;
    if (lastName != null) data['last_name'] = lastName;

    return data;
  }
}
