import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';
import 'package:flutter_nanolike_sdk/domain/models/filters.dart';

class GetDashboardRequest extends RequestPaginated {
  final Filters? filters;

  GetDashboardRequest({super.queryParam, this.filters});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => "assets/mocks/dashboard.json";

  @override
  String get path => '/v2/dashboard/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams {
    final params = super.queryParam.toMap();

    final groupsUnWrap = filters?.groups;
    if (groupsUnWrap != null && groupsUnWrap.isNotEmpty) {
      params["group_ids"] = groupsUnWrap.map((e) => e.id).join(',');
    }

    final deviceContentsUnWrap = filters?.deviceContents;
    if (deviceContentsUnWrap != null && deviceContentsUnWrap.isNotEmpty) {
      params["device_content_id"] =
          deviceContentsUnWrap.map((e) => e.id).join(',');
    }

    final levelsUnWrap = filters?.levels;
    if (levelsUnWrap != null && levelsUnWrap.isNotEmpty) {
      params["level_filters"] = levelsUnWrap.join(',');
    }

    final statusUnWrap = filters?.statuses;
    if (statusUnWrap != null && statusUnWrap.isNotEmpty) {
      params["status"] = statusUnWrap.join(',');
    }

    final remainingDaysUnWrap = filters?.remainingDays;
    if (remainingDaysUnWrap != null && remainingDaysUnWrap.isNotEmpty) {
      params["remaining_days"] = remainingDaysUnWrap.join(',');
    }

    if (filters?.levelTonsLessThan != null) {
      params["level_t_lt"] = filters?.levelTonsLessThan;
    }

    if (filters?.levelTonsGreaterThan != null) {
      params["level_t_gt"] = filters?.levelTonsGreaterThan;
    }

    if (filters?.missingWeightLessThan != null) {
      params["missing_weight_lt"] = filters?.missingWeightLessThan;
    }

    if (filters?.missingWeightGreaterThan != null) {
      params["missing_weight_gt"] = filters?.missingWeightGreaterThan;
    }

    return params;
  }
}
