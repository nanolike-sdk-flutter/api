import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class DeleteGroupRequest extends Request {
  final String _groupId;

  DeleteGroupRequest(this._groupId);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'DELETE';

  @override
  String get mock => "assets/mocks/empty.json";

  @override
  String get path => '/v1/groups/$_groupId/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
