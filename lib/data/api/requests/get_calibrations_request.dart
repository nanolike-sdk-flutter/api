import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';
import 'package:flutter_nanolike_sdk/domain/models/calibration.dart';

class GetCalibrationsPaginatedRequest extends RequestPaginated {
  final String? deviceReference;
  final String? groupId;
  final DateTime? start;
  final DateTime? end;
  final CalibrationType? type;

  GetCalibrationsPaginatedRequest(
      {this.deviceReference,
      this.groupId,
      this.start,
      this.end,
      this.type,
      super.queryParam});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => 'assets/mocks/calibrations.json';

  @override
  String get path => '/v2/calibrations';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams {
    final params = super.queryParam.toMap();

    if (deviceReference != null) {
      params["idDevice"] = deviceReference;
    }

    if (groupId != null) {
      params["idGroup"] = groupId;
    }

    if (start != null) {
      params["start_date"] = start!.toIso8601String();
    }

    if (end != null) {
      params["end_date"] = end!.toIso8601String();
    }

    if (type != null) {
      params["type"] = type.toString().split('.').last;
    }

    return params;
  }
}
