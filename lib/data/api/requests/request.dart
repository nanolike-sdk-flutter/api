enum AuthorizationType { basic, bearer }

abstract class Request {
  String get path;
  String get method;
  dynamic get data;
  Map<String, dynamic>? get header;
  Map<String, dynamic>? get queryParams;
  AuthorizationType get authorizationType;
  bool get needWorkspaceHeader => true;
  String get mock;
}
