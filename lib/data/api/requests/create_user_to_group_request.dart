import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class CreateUserToGroupRequest extends Request {
  final String userId;
  final String groupId;

  CreateUserToGroupRequest({required this.userId, required this.groupId});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => {'group_id': groupId};

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/empty.json';

  @override
  String get path => '/v1/users/$userId/group-memberships';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
