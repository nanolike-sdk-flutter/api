import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetDeviceMediasRequest extends Request {
  final String _deviceId;

  GetDeviceMediasRequest(this._deviceId);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/device_medias.json';

  @override
  String get path => '/v2/devices/$_deviceId/medias';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}