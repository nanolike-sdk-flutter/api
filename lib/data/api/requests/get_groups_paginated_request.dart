import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';

class GetGroupsPaginatedRequest extends RequestPaginated {
  final bool isPoi;

  GetGroupsPaginatedRequest({super.queryParam, required this.isPoi});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => 'assets/mocks/groups_paginated.json';

  @override
  String get path => '/v1/groups/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic> get queryParams {
    final params = super.queryParam.toMap();
    if (isPoi) params["is_poi"] = true;
    params["ordering"] = "group_name";
    return params;
  }
}
