import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_operation_type.dart';

class MaintenanceDeviceRequest extends Request {
  final String deviceToMaintainReference;
  final DeviceOperationType operationType;
  final String? comment;

  MaintenanceDeviceRequest(
      {required this.deviceToMaintainReference,
      required this.operationType,
      this.comment});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => {
        'operationType': operationType.serverString,
        if (comment?.isNotEmpty ?? false) 'comment': comment,
      };

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/empty.json';

  @override
  String get path => '/v2/devices/$deviceToMaintainReference/maintenance';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
