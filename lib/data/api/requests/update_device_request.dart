import 'package:flutter_nanolike_sdk/data/api/requests/payload/device_payload.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class UpdateDeviceRequest extends Request {
  final String deviceReference;
  final DevicePayload devicePayload;

  UpdateDeviceRequest({
    required this.deviceReference,
    required this.devicePayload,
  });

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => devicePayload.toJson();

  @override
  String get method => 'PUT';

  @override
  String get mock => 'assets/mocks/device.json';

  @override
  String get path => '/v1/devices/$deviceReference';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
