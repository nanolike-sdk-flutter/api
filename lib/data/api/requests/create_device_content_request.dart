import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class CreateDeviceContentRequest extends Request {
  final String deviceContent;

  CreateDeviceContentRequest(this.deviceContent);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => {"device_content": deviceContent};

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/empty.json';

  @override
  String get path => '/v1/workspace/device-contents/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
