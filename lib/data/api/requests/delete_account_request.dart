import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class DeleteAccountRequest extends Request {
  final String userId;

  DeleteAccountRequest(this.userId);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'DELETE';

  @override
  String get mock => "assets/mocks/empty.json";

  @override
  String get path => '/v1/users/$userId/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
