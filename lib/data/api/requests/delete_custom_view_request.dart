import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class DeleteCustomViewRequest extends Request {
  final String customViewId;

  DeleteCustomViewRequest(this.customViewId);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'DELETE';

  @override
  String get mock => "assets/mocks/empty.json";

  @override
  String get path => '/v2/custom-views/$customViewId/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
