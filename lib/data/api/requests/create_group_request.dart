import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class CreateGroupRequest extends Request {
  final String name;
  final String? customId;
  final String? street;
  final String? town;
  final String? zipCode;
  final String? comment;
  final String? ownerEmail;
  final List<String> orderRecipients;

  CreateGroupRequest(
      {required this.name,
      required this.customId,
      required this.street,
      required this.town,
      required this.zipCode,
      required this.comment,
      required this.ownerEmail,
      required this.orderRecipients});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => {
        'client_poi_id': customId,
        'group_name': name,
        'is_poi': true,
        'address_zipcode': zipCode,
        'address_town': town,
        'address_street': street,
        'comment': comment,
        if (ownerEmail != null && ownerEmail!.isNotEmpty)
          'owner_email': ownerEmail,
        'order_recipient_emails': orderRecipients
      };

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/post_group.json';

  @override
  String get path => '/v1/groups/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
