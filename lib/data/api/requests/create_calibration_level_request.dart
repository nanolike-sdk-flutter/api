import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class CreateCalibrationLevelRequest extends Request {
  final String deviceId;
  final DateTime? date;
  final num level;

  CreateCalibrationLevelRequest(
      {required this.deviceId, required this.level, this.date});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => {
        'calibration_level': level.toDouble(),
        'calibration_date': date?.toUtc().toIso8601String() ??
            DateTime.now().toUtc().toIso8601String()
      };

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/empty.json';

  @override
  String get path => '/v1/devices/$deviceId/calibrate';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
