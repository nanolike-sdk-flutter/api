import 'package:dio/dio.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class CreateDeviceMediaRequest extends Request {
  final String _deviceId;
  final FormData _data;

  CreateDeviceMediaRequest(this._deviceId, this._data);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => _data;

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/device_media.json';

  @override
  String get path => '/v2/devices/$_deviceId/medias';

  @override
  Map<String, dynamic>? get header => {
    'Content-Type': "multipart/form-data"
  };

  @override
  Map<String, dynamic>? get queryParams => null;
}