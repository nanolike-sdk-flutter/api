import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';

class GetCustomViewsPaginatedRequest extends RequestPaginated {
  GetCustomViewsPaginatedRequest({super.queryParam});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => 'assets/mocks/custom_views_paginated.json';

  @override
  String get path => '/v2/custom-views';

  @override
  Map<String, dynamic>? get header => null;
}
