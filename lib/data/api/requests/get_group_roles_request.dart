import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetGroupRolesRequest extends Request {
  GetGroupRolesRequest();

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/group_acl_roles.json';

  @override
  String get path => '/v1/group-acl-roles/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
