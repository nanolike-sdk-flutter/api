import 'dart:convert';

import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/domain/models/order.dart';

class UpdateOrderRequest extends Request {
  final Order _order;

  UpdateOrderRequest(this._order);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => jsonEncode(_order);

  @override
  String get method => 'PUT';

  @override
  String get mock => 'assets/mocks/order.json';

  @override
  String get path => '/v1/orders/${_order.id}';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
