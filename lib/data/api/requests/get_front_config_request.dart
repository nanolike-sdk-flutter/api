import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetFrontConfigRequest extends Request {
  GetFrontConfigRequest();

  @override
  AuthorizationType get authorizationType => AuthorizationType.basic;

  @override
  bool get needWorkspaceHeader => false;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/front-config.json';

  @override
  String get path => '/v2/front-config';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
