import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class ResetPasswordRequest extends Request {
  final String email;
  final String language;

  ResetPasswordRequest(this.email, this.language);

  @override
  AuthorizationType get authorizationType => AuthorizationType.basic;

  @override
  bool get needWorkspaceHeader => false;

  @override
  get data => jsonBody(email, language);

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/empty.json';

  @override
  String get path => '/v1/reset-password';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;

  Map<String, dynamic> jsonBody(String email, String language) {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['lang'] = language;
    return data;
  }
}
