import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetGroupAccessoriesRequest extends Request {
  final String groupId;

  GetGroupAccessoriesRequest(this.groupId);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/accessories.json';

  @override
  String get path => '/v1/accessories/$groupId';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
