class AuthorizationHelper {
  bool hasAuthorization(Map<String, dynamic>? authorizationServer,
      String resource, List<String> operations) {
    // no authorization
    if (authorizationServer == null) return false;

    // no authorization for this ressource
    final List<String>? operationsServer =
        authorizationServer[resource]?.cast<String>();
    if (operationsServer == null || operationsServer.isEmpty) return false;

    // full access or specific authorization for this ressource
    if (operationsServer.contains('*') ||
        operationsServer.any(operations.contains)) {
      return true;
    }

    // no specific authorization for this ressource
    return false;
  }
}
