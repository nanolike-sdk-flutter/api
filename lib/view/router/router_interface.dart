import 'package:flutter/material.dart';

abstract class IRouter {
  RouterConfig<Object> get routerConfig;
}
