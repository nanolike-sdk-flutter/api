import 'dart:async';
import 'dart:developer';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_nanolike_sdk/domain/models/filters.dart';
import 'package:flutter_nanolike_sdk/domain/models/front_config.dart';
import 'package:flutter_nanolike_sdk/domain/models/nanolike_exception.dart';
import 'package:flutter_nanolike_sdk/domain/models/session.dart';
import 'package:flutter_nanolike_sdk/domain/models/token.dart';
import 'package:flutter_nanolike_sdk/domain/models/user.dart';
import 'package:flutter_nanolike_sdk/domain/models/role.dart';
import 'package:flutter_nanolike_sdk/domain/models/workspace.dart';
import 'package:flutter_nanolike_sdk/domain/models/workspace_settings.dart';
import 'package:flutter_nanolike_sdk/domain/presenters/session/session_presenter.dart';
import 'package:flutter_nanolike_sdk/domain/providers/api_provider.dart';
import 'package:flutter_nanolike_sdk/domain/providers/storage_provider.dart';
import 'package:jwt_decode/jwt_decode.dart';

class SessionPresenterImplementation implements ISessionPresenter {
  final IWebApiJWTProvider _apiProvider;
  final IStorageProvider _storageProvider;
  final StreamController<User?> _meStreamController = StreamController<User?>();

  User? _me;

  SessionPresenterImplementation._(this._apiProvider, this._storageProvider) {
    log("SessionPresenterImplementation: init");
    _apiProvider.jwtTokenStream.listen((token) async {
      log("SessionPresenterImplementation: jwtTokenStream listen $token");
      if (token == null) {
        await _storageProvider
            .removeAll()
            .whenComplete(() => _updateMeStreamController(null));
      } else {
        final me = await _apiProvider.getMe();
        await _storageProvider
            .saveJWTToken(token)
            .whenComplete(() => _updateMeStreamController(me));
      }
    });
  }

  void _updateMeStreamController(User? user) {
    // if same user with same role and menu, no need to update the stream.
    // stream is only use by the router and the router for navigation.
    if (user != null &&
        _me?.id == user.id &&
        _me?.workspaceRole?.id == user.workspaceRole?.id &&
        listEquals(_me?.menu, user.menu)) {
      log('SessionPresenterImplementation: stream NOT updated same user');
      return;
    }

    log('SessionPresenterImplementation: stream updated for ${user?.email}');
    _me = user;
    _meStreamController.add(user);
  }

  static Future<SessionPresenterImplementation> start(
      IWebApiJWTProvider apiProvider, IStorageProvider storageProvider) async {
    final sessionProvider =
        SessionPresenterImplementation._(apiProvider, storageProvider);

    // to reset the session, pass `--dart-define=resetSession=true` during the run/build/drive.
    const resetSession = bool.fromEnvironment("resetSession");
    if (resetSession) {
      log("SessionPresenterImplementation: Reset session");
      await sessionProvider.logout();
    }

    final token = await sessionProvider._storageProvider.getJWTToken();

    if (token != null) {
      try {
        final me = await apiProvider.getMe();
        sessionProvider._updateMeStreamController(me);
      } catch (error) {
        log("SessionPresenterImplementation: getMe error: $error");
        sessionProvider._updateMeStreamController(null);
      }
    } else {
      sessionProvider._updateMeStreamController(null);
    }

    return sessionProvider;
  }

  @override
  Future<void> logout() async {
    _apiProvider.logout();
  }

  @override
  Future<void> login(String login, String password) async {
    try {
      final JWTToken token =
          await _apiProvider.signIn(login: login, password: password);
      await _storageProvider.saveJWTToken(token);
      final me = await _apiProvider.getMe();
      _updateMeStreamController(me);
    } on DioException catch (error) {
      final String? dioErrorCodeString = error.response?.data["code"];

      if (dioErrorCodeString != null) {
        if (dioErrorCodeString == "account_deactivated") {
          throw NanolikeException(
              type: NanolikeExceptionType.accountDeactivated);
        } else if (dioErrorCodeString == "workspace_mismatch") {
          throw NanolikeException(
              type: NanolikeExceptionType.workspaceMismatch);
        } else if (dioErrorCodeString == "bad_credentials") {
          throw NanolikeException(type: NanolikeExceptionType.badCredentials);
        } else if (dioErrorCodeString == "no_client_existing") {
          throw NanolikeException(type: NanolikeExceptionType.noClientExisting);
        }
      }
      throw NanolikeException(type: NanolikeExceptionType.other);
    } on Exception {
      throw NanolikeException(type: NanolikeExceptionType.other);
    }
  }

  @override
  Future<void> resetPassword(String email, String language) async {
    try {
      await _apiProvider.resetPassword(email, language);
    } catch (error) {
      if (error is DioException) {
        if (error.response?.statusCode == 404) {
          throw NanolikeException(
              type: NanolikeExceptionType.forgotPasswordEmailNotFound);
        } else if (error.response?.statusCode == 400) {
          throw NanolikeException(
              type: NanolikeExceptionType.forgotPasswordWorkspaceNotFound);
        } else if (error.response?.statusCode == 403) {
          throw NanolikeException(
              type: NanolikeExceptionType.noPermissionToForgotPassword);
        }
      }
      throw NanolikeException(type: NanolikeExceptionType.other);
    }
  }

  @override
  Future<void> register(
      {required String token,
      required String email,
      required String password}) async {
    try {
      await _apiProvider.register(token, password);
    } catch (error) {
      throw NanolikeException(type: NanolikeExceptionType.other);
    }
    await login(email, password);
  }

  @override
  Future<String> validateRegisterToken({required String token}) async {
    return await _apiProvider.validateRegisterToken(token);
  }

  @override
  Stream<User?> get meStream => _meStreamController.stream;

  @override
  User? get currentMe => _me;

  @override
  Future<List<Role>> getUserRoles() async {
    return await _apiProvider.getUserRoles();
  }

  @override
  Future<WorkspaceSettings> workspaceSettings() async {
    try {
      return await _apiProvider.workspaceSettings();
    } on DioException catch (error) {
      final String? dioErrorCodeString = error.response?.data["code"];

      if (dioErrorCodeString != null) {
        if (dioErrorCodeString == "workspace_mismatch") {
          throw NanolikeException(
              type: NanolikeExceptionType.unauthorizedWorkspace);
        }
      }
      throw NanolikeException(type: NanolikeExceptionType.other);
    } on Exception {
      throw NanolikeException(type: NanolikeExceptionType.other);
    }
  }

  @override
  Future<List<Role>> getGroupRoles() async {
    return await _apiProvider.getGroupRoles();
  }

  @override
  Future<String> getTermsAndConditions(String language) async {
    return await _apiProvider.getTermsAndConditions(language);
  }

  @override
  Future<User> getMe() async {
    final me = await _apiProvider.getMe();
    _updateMeStreamController(me);
    return me;
  }

  @override
  Future<List<Workspace>> getWorkspaces() async {
    return await _apiProvider.getWorkspaces();
  }

  @override
  Future<FrontConfig> getFrontConfig() async {
    return await _apiProvider.getFrontConfig();
  }

  @override
  Future<String?> getCurrentWorkspaceName() async {
    final session = await _storageProvider.getSession();
    return session?.workspace;
  }

  @override
  Future<void> updateCurrentWorkspaceName(String workspace) async {
    _apiProvider.updateWorkspaceHeader(workspace);
    return await _storageProvider.saveSession(Session(workspace));
  }

  @override
  Future<void> addFirebaseToken(String token) async {
    final userId = await _getUserId();
    if (userId != null) {
      return await _apiProvider.addFirebaseToken(userId, token);
    }
  }

  @override
  Future<void> deleteFirebaseToken(String token) async {
    final userId = await _getUserId();
    if (userId != null) {
      return await _apiProvider.deleteFirebaseToken(userId, token);
    }
  }

  @override
  Future<Filters?> getFilters() async {
    return await _storageProvider.getFilters();
  }

  @override
  Future<void> saveFilters({Filters? filters}) async {
    return await _storageProvider.saveFilters(filters: filters);
  }

  @override
  Future<User> updatePassword(
      {required String oldPassword, required String newPassword}) async {
    final userId = await _getUserId();

    if (userId == null) {
      throw NanolikeException(type: NanolikeExceptionType.other);
    }

    try {
      return await _apiProvider.updateUser(
          userId: userId, oldPassword: oldPassword, newPassword: newPassword);
    } on DioException catch (error) {
      final String? dioErrorMessagetring = error.response?.data["message"];

      if (dioErrorMessagetring != null) {
        if (dioErrorMessagetring.contains("new_password")) {
          throw NanolikeException(type: NanolikeExceptionType.wrongNewPassword);
        }
        if (dioErrorMessagetring.contains("old_password")) {
          throw NanolikeException(type: NanolikeExceptionType.wrongOldPassword);
        }
      }
      throw NanolikeException(type: NanolikeExceptionType.other);
    } on Exception {
      throw NanolikeException(type: NanolikeExceptionType.other);
    }
  }

  @override
  Future<void> deleteMe() async {
    final userId = await _getUserId();

    if (userId == null) {
      throw NanolikeException(type: NanolikeExceptionType.other);
    }

    return await _apiProvider.deleteAccount(userId);
  }

  @override
  Future<User> updatePreferences(
      {bool? notificationPush,
      bool? notificationEmail,
      Locale? preferredLanguage,
      bool? displayForecast,
      bool? displayRemaining,
      bool? displayLastOrder,
      bool? displayQuickActionOrder}) async {
    final userId = await _getUserId();

    if (userId == null) {
      throw NanolikeException(type: NanolikeExceptionType.other);
    }

    return await _apiProvider.updateUser(
        userId: userId,
        notificationPush: notificationPush,
        notificationEmail: notificationEmail,
        preferredLanguage: preferredLanguage,
        displayForecast: displayForecast,
        displayRemaining: displayRemaining,
        displayLastOrder: displayLastOrder,
        displayQuickActionOrder: displayQuickActionOrder);
  }

  @override
  Future<User> updateProfile({String? firstName, String? lastName}) async {
    final userId = await _getUserId();

    if (userId == null) {
      throw NanolikeException(type: NanolikeExceptionType.other);
    }

    return await _apiProvider.updateUser(
        userId: userId, firstName: firstName, lastName: lastName);
  }

  Future<String?> _getUserId() async {
    final jwtToken = await _storageProvider.getJWTToken();
    if (jwtToken != null) {
      Map<String, dynamic> payload = Jwt.parseJwt(jwtToken.accessToken);
      return payload["user_id"];
    }
    return null;
  }
}
