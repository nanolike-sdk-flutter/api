import 'dart:ui';

import 'package:flutter_nanolike_sdk/domain/models/filters.dart';
import 'package:flutter_nanolike_sdk/domain/models/front_config.dart';
import 'package:flutter_nanolike_sdk/domain/models/user.dart';
import 'package:flutter_nanolike_sdk/domain/models/role.dart';
import 'package:flutter_nanolike_sdk/domain/models/workspace.dart';
import 'package:flutter_nanolike_sdk/domain/models/workspace_settings.dart';

abstract class ISessionPresenter {
  Future<FrontConfig> getFrontConfig();
  Future<String> getTermsAndConditions(String language);
  Future<void> login(String login, String password);
  Future<String> validateRegisterToken({required String token});
  Future<void> register(
      {required String token, required String email, required String password});
  Future<void> resetPassword(String email, String language);
  Future<void> logout();
  Future<void> deleteMe();
  Stream<User?> get meStream;
  User? get currentMe;
  Future<List<Role>> getUserRoles();
  Future<List<Role>> getGroupRoles();
  Future<User> getMe();
  Future<List<Workspace>> getWorkspaces();
  Future<String?> getCurrentWorkspaceName();
  Future<void> updateCurrentWorkspaceName(String workspace);
  Future<void> addFirebaseToken(String token);
  Future<void> deleteFirebaseToken(String token);
  Future<WorkspaceSettings> workspaceSettings();
  Future<void> saveFilters({Filters? filters});
  Future<Filters?> getFilters();
  Future<User> updateProfile({String? firstName, String? lastName});
  Future<User> updatePassword(
      {required String oldPassword, required String newPassword});
  Future<User> updatePreferences(
      {bool? notificationPush,
      bool? notificationEmail,
      Locale? preferredLanguage,
      bool? displayForecast,
      bool? displayRemaining,
      bool? displayLastOrder,
      bool? displayQuickActionOrder});
}
