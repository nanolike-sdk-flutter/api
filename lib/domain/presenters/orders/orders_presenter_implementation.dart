import 'package:dio/dio.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_content.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_content_type.dart';
import 'package:flutter_nanolike_sdk/domain/models/drug.dart';
import 'package:flutter_nanolike_sdk/domain/models/error_response.dart';
import 'package:flutter_nanolike_sdk/domain/models/nanolike_exception.dart';
import 'package:flutter_nanolike_sdk/domain/models/order.dart';
import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';
import 'package:flutter_nanolike_sdk/domain/models/supplement.dart';
import 'package:flutter_nanolike_sdk/domain/presenters/orders/orders_presenter.dart';
import 'package:flutter_nanolike_sdk/domain/providers/api_provider.dart';

class OrdersPresenterImplementation implements IOrdersPresenter {
  final IWebApiJWTProvider _apiProvider;

  OrdersPresenterImplementation(this._apiProvider);

  @override
  Future<List<Order>> getOrders() async {
    try {
      return await _apiProvider.getOrders();
    } catch (error) {
      if (error is DioException && error.response?.statusCode == 403) {
        throw NanolikeException(type: NanolikeExceptionType.dashboardForbidden);
      }
      throw NanolikeException(type: NanolikeExceptionType.other);
    }
  }

  @override
  Future<Order> getOrder(String orderId) async {
    try {
      return await _apiProvider.getOrder(orderId);
    } catch (error) {
      if (error is DioException && error.response?.statusCode == 403) {
        throw NanolikeException(type: NanolikeExceptionType.dashboardForbidden);
      }
      throw NanolikeException(type: NanolikeExceptionType.orderNotFound);
    }
  }

  @override
  Future<void> createOrder(Order order) async {
    try {
      await _apiProvider.createOrder(order);
    } catch (error) {
      if (error is DioException && error.response?.data != null) {
        if (ErrorResponse.fromJson(error.response?.data).code ==
            "order_already_exist_for_silo_date") {
          throw NanolikeException(
              type: NanolikeExceptionType.orderAlreadyExistForSiloDate);
        }
      }
      throw NanolikeException(type: NanolikeExceptionType.other);
    }
  }

  @override
  Future<void> updateOrder(Order order) async {
    return await _apiProvider.updateOrder(order);
  }

  @override
  Future<void> createDeviceContent(String deviceContent) async {
    return await _apiProvider.createDeviceContent(deviceContent);
  }

  @override
  Future<PaginatedData<DeviceContent>> getDeviceContentsPaginated(
      QueryParam params) async {
    return await _apiProvider.getDeviceContentsPaginated(params);
  }

  @override
  Future<PaginatedData<DeviceContentType>> getDeviceContentTypesPaginated(
      QueryParam params) async {
    return await _apiProvider.getDeviceContentTypesPaginated(params);
  }

  @override
  Future<PaginatedData<Drug>> getDrugsPaginated(QueryParam params) async {
    return await _apiProvider.getDrugsPaginated(params);
  }

  @override
  Future<PaginatedData<Supplement>> getSupplementsPaginated(
      QueryParam params) async {
    return await _apiProvider.getSupplementsPaginated(params);
  }
}
