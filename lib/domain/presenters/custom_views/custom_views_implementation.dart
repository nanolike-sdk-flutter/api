import 'package:flutter_nanolike_sdk/domain/models/custom_view.dart';
import 'package:flutter_nanolike_sdk/domain/models/filters.dart';
import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';
import 'package:flutter_nanolike_sdk/domain/presenters/custom_views/custom_views_presenter.dart';
import 'package:flutter_nanolike_sdk/domain/providers/api_provider.dart';

class CustomViewsPresenterImplementation implements ICustomViewsPresenter {
  final IWebApiJWTProvider _apiProvider;

  CustomViewsPresenterImplementation(this._apiProvider);

  @override
  Future<CustomView> createCustomView(String name, Filters filters) async {
    return await _apiProvider.createCustomView(name, filters);
  }

  @override
  Future<CustomView> getCustomView(String customViewId) async {
    return await _apiProvider.getCustomView(customViewId);
  }

  @override
  Future<void> deleteCustomView(String customViewId) async {
    return await _apiProvider.deleteCustomView(customViewId);
  }

  @override
  Future<PaginatedData<CustomView>> getCustomViewsPaginated(
      QueryParam params) async {
    return await _apiProvider.getCustomViewsPaginated(params);
  }

  @override
  Future<CustomView> updateCustomView(
      String customViewId, String name, Filters filters) async {
    return await _apiProvider.updateCustomView(customViewId, name, filters);
  }
}
