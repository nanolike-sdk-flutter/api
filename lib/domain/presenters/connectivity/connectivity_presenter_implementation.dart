import 'package:dio/dio.dart';
import 'package:flutter_nanolike_sdk/domain/models/connectivity_message.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_operation_type.dart';
import 'package:flutter_nanolike_sdk/domain/models/nanolike_exception.dart';
import 'package:flutter_nanolike_sdk/domain/providers/api_provider.dart';

import 'connectivity_presenter.dart';

class ConnectivityPresenterImplementation implements IConnectivityPresenter {
  final IWebApiJWTProvider _apiProvider;

  ConnectivityPresenterImplementation(this._apiProvider);

  @override
  Future<List<ConnectivityMessage>> fetchMessages(
      {required String deviceReference,
      ({
        String deviceToMaintainReference,
        DeviceOperationType deviceOperationType
      })? maintenance}) async {
    String? deviceToMaintainReference;
    if (maintenance != null &&
        (maintenance.deviceOperationType == DeviceOperationType.newElec)) {
      deviceToMaintainReference = maintenance.deviceToMaintainReference;
    }
    try {
      return await _apiProvider.fetchConnectivityMessages(
          deviceReference: deviceReference,
          deviceToMaintainReference: deviceToMaintainReference);
    } catch (error) {
      final httpError = error as DioException?;
      final statusCode = httpError?.response?.statusCode;
      switch (statusCode) {
        case 404:
          {
            if (httpError?.response?.data['code'] == "NOT_PROVISIONED") {
              throw NanolikeException(
                  type: NanolikeExceptionType.deviceNotProvisioned);
            } else {
              throw NanolikeException(
                  type: NanolikeExceptionType.deviceNotFound);
            }
          }
        default:
          {
            rethrow;
          }
      }
    }
  }
}
