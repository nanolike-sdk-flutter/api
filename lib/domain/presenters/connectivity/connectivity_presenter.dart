import 'package:flutter_nanolike_sdk/domain/models/connectivity_message.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_operation_type.dart';

abstract class IConnectivityPresenter {
  Future<List<ConnectivityMessage>> fetchMessages(
      {required String deviceReference,
      ({
        String deviceToMaintainReference,
        DeviceOperationType deviceOperationType
      })? maintenance});
}
