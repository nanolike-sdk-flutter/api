import 'dart:ui';

import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';
import 'package:flutter_nanolike_sdk/domain/models/role.dart';
import 'package:flutter_nanolike_sdk/domain/models/group.dart';
import 'package:flutter_nanolike_sdk/domain/models/user.dart';

abstract class IGroupsPresenter {
  Future<PaginatedData<Group>> fetchGroupsPaginated(QueryParam params);
  Future<PaginatedData<Group>> fetchGroupsPoiPaginated(QueryParam params);
  Future<Group> fetchGroup(String groupId);
  Future<User> invite(
      {required String email,
      String? firstName,
      String? lastName,
      required List<String> groupIds,
      required Locale preferredLanguage,
      required Role role});
  Future<Group> createGroup(
      final String name,
      final String? customId,
      final String? postalAddress,
      final String? zipCode,
      final String? town,
      final String? comment,
      final String? ownerEmail,
      final List<String> orderRecipients);
  Future<Group> updateGroup(
    final String groupId,
    final String? name,
    final String? customId,
    final String? postalAddress,
    final String? zipCode,
    final String? town,
    final String? comment,
  );
  Future<void> deleteGroup(String groupId);
}
