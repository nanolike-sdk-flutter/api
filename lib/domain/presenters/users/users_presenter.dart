import 'dart:ui';

import 'package:flutter_nanolike_sdk/domain/models/group.dart';
import 'package:flutter_nanolike_sdk/domain/models/group_membership.dart';
import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';
import 'package:flutter_nanolike_sdk/domain/models/role.dart';
import 'package:flutter_nanolike_sdk/domain/models/user.dart';

abstract class IUsersPresenter {
  Future<User> getUser(String id);
  Future<void> deleteUser(String id);
  Future<User> reinvite(String id);
  Future<PaginatedData<User>> getUsersPaginated(QueryParam params);
  Future<void> addUserToGroup({required User user, required Group group});
  Future<void> deleteUserFromGroupMembership(
      {required User user, required GroupMembership groupMembership});
  Future<User> updateUser(
      {required String userId,
      String? firstName,
      String? lastName,
      Locale? preferredLanguage,
      String? roleId});
  Future<List<Role>> getMeRoles();
  Future<PaginatedData<Role>> getMeRolesPaginated(QueryParam params);
  Future<PaginatedData<Locale>> getLanguages(QueryParam params);
}
