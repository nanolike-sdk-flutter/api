import 'package:flutter_nanolike_sdk/domain/models/accessory.dart';

abstract class IAccessoriesPresenter {
  Future<List<Accessory>> fetchAccessories(String groupId);
  Future<Accessory> createAccessory(
      {required String groupId,
      required String accessoryId,
      required AccessoryType accessoryType});
  Future<void> deleteAccessory({required int accessoryId});
}
