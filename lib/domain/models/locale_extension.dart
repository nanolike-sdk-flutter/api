import 'dart:ui';

extension LocaleExtensions on Locale {
  String get flagUrl {
    String code = languageCode;
    switch (toString()) {
      case 'zh_CH':
        code = 'cn';
        break;
      case 'sv':
        code = 'se';
        break;
      case 'zh_TW':
        code = 'tw';
        break;
      case 'ja':
        code = 'jp';
        break;
      case 'vi':
        code = 'vn';
        break;
      case 'en':
        code = 'gb';
        break;
      case 'en_US':
        code = 'us';
        break;
      case 'da':
        code = 'dk';
        break;
      case 'en_CA':
      case 'fr_CA':
        code = 'ca';
        break;
    }
    return 'https://flagcdn.com/24x18/$code.png';
  }

  String? get languageName {
    final locale = toString();

    String? getLanguageName(String locale) {
      switch (locale) {
        case 'da':
          return 'Danske';
        case 'de':
          return 'Deutsch';
        case 'en_CA':
          return 'English (Canada)';
        case 'en_US':
          return 'English (US)';
        case 'en':
          return 'English';
        case 'es':
          return 'Español';
        case 'fr':
          return 'Français';
        case 'fr_CA':
          return 'Français (Canada)';
        case 'it':
          return 'Italiano';
        case 'pl':
          return 'Polski';
        case 'pt':
          return 'Português';
        case 'sv':
          return 'Svenska';
        case 'vi':
          return 'Tiếng Việt';
        case 'th':
          return 'ไทย';
        case 'ru':
          return 'Русский';
        case 'zh_CH':
          return '简体中文';
        case 'zh_TW':
          return '簡體中文（台灣）';
        case 'ja':
          return '日本語';
        default:
          return null;
      }
    }

    String? language = getLanguageName(locale);
    if (language != null) {
      return language;
    }

    final baseLocale = locale.split('_').first;
    return getLanguageName(baseLocale);
  }
}

class LocaleUtils {
  static Locale fromString(String localeString) {
    final parts = localeString.split(RegExp('[_-]'));
    if (parts.length == 1) {
      return Locale(parts[0]);
    } else if (parts.length == 2) {
      return Locale(parts[0], parts[1]);
    } else {
      return const Locale('en');
    }
  }
}
