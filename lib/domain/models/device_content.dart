class DeviceContent {
  final String id;
  final String name;

  DeviceContent({required this.id, required this.name});

  factory DeviceContent.fromAPIJson(Map<String, dynamic> json) {
    return DeviceContent(id: json['id'], name: json['device_content']);
  }

  factory DeviceContent.fromJson(Map<String, dynamic> json) {
    return DeviceContent(id: json['id'], name: json['name']);
  }

  Map<String, dynamic> toJson() {
    return {'id': id, 'name': name};
  }

  @override
  bool operator ==(Object other) =>
      other is DeviceContent &&
      other.runtimeType == runtimeType &&
      (other.id == id || other.name == name);

  @override
  int get hashCode => id.hashCode;
}
