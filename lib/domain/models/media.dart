class Media {

  final String idMedia;
  final String idDevice;
  final String url;
  final String key;

  Media(this.idMedia, this.idDevice, this.url, this.key);

  factory Media.fromJson(Map<String, dynamic> json) {
    return Media(
      json['idMedia'],
      json['idDevice'],
      json['url'],
      json['key'],
    );
  }
}