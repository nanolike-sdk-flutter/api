enum DeviceWarningCause { ice, unknown }

extension DeviceWarningCauseFactory on DeviceWarningCause {
  static DeviceWarningCause from(String value) {
    switch (value) {
      case 'ice':
        return DeviceWarningCause.ice;
      default:
        return DeviceWarningCause.unknown;
    }
  }
}
