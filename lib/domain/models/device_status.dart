enum DeviceStatus {
  ok,
  calibrating,
  pending,
  problem,
  error,
  levelProblem,
  calibrationProblem,
  unknown
}
