import 'dart:core';

import 'package:flutter_nanolike_sdk/domain/models/device_battery_status.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_content.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_content_type.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_operation_type.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_status.dart';
import 'package:flutter_nanolike_sdk/domain/models/graph_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/group.dart';
import 'package:flutter_nanolike_sdk/domain/models/ibc_model.dart';
import 'package:flutter_nanolike_sdk/domain/models/info_to_calibrate.dart';
import 'package:flutter_nanolike_sdk/domain/models/location.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_warning_cause.dart';
import 'package:flutter_nanolike_sdk/domain/models/workspace.dart';

class Device implements Comparable {
  // Mandatory
  final String id;
  final String reference;
  final String name;
  // Optional
  final Group? groupPoi;
  final DeviceBatteryStatus? batteryStatus;
  final DeviceWarningCause? warningCause;
  final DeviceStatus? status;
  final DateTime? installDate;
  final DateTime? lastDeliveryDate;
  final DateTime? nextDeliveryDate;
  final DateTime? lastMessageReceivedDate;
  final DateTime? lastCalibrationDate;
  final num? lastTotalOrderInTons;
  final Location? position;
  final num? capacity;
  final double? fixedLat;
  final double? fixedLong;
  final List<String>? groupIds;
  final int? siloLegs;
  final DeviceContent? deviceContent;
  final DeviceContentType? deviceContentType;
  final List<InfoToCalibrate>? infosToCalibrate;
  final DeviceOperationType? operationSuggested;
  final num? remainingDays;
  final num? mean15Days;
  final num? max15Days;
  List<DataPointLevel>? levels;
  final bool? isCombined;
  final bool? isFake;
  final bool? isTank;
  final bool? isSilo;
  final List<Device>? devicesCombined;
  final bool? hasParentCombinedDevice;
  final String? color;
  final String? serialNumber;
  final DateTime? deviceFabricationDate;
  final IBCModel? ibcModel;
  final Workspace? client;

  Device(
      {required this.id,
      required this.reference,
      required this.name,
      this.groupPoi,
      this.batteryStatus,
      this.warningCause,
      this.status,
      this.installDate,
      this.lastDeliveryDate,
      this.nextDeliveryDate,
      this.lastMessageReceivedDate,
      this.lastCalibrationDate,
      this.lastTotalOrderInTons,
      this.infosToCalibrate,
      this.operationSuggested,
      this.position,
      this.capacity,
      this.fixedLat,
      this.fixedLong,
      this.groupIds,
      this.siloLegs,
      this.deviceContent,
      this.deviceContentType,
      this.remainingDays,
      this.mean15Days,
      this.max15Days,
      this.levels,
      this.isCombined,
      this.isFake,
      this.isTank,
      this.isSilo,
      this.devicesCombined,
      this.hasParentCombinedDevice,
      this.color,
      this.serialNumber,
      this.deviceFabricationDate,
      this.ibcModel,
      this.client});

  Location? get location =>
      position ??
      ((fixedLat != null && fixedLong != null)
          ? Location(fixedLat!, fixedLong!)
          : null);

  factory Device.fromDashboardJson(Map<String, dynamic> json) {
    // Parse Position
    Location? position;
    Map<String, dynamic>? positionJson = json['position'];
    if (positionJson != null) {
      double? latitude = positionJson['position_latitude'];
      double? longitude = positionJson['position_longitude'];
      if (latitude != null && longitude != null) {
        position = Location(latitude, longitude);
      }
    }

    final lastCalibration = json['last_calibration'];
    DateTime? lastCalibrationDate;

    if (lastCalibration != null) {
      if (lastCalibration['value'] != null) {
        lastCalibrationDate =
            DateTime.tryParse(lastCalibration['timestamp']?.toString() ?? "");
      }
    }

    List<dynamic>? jsonLevels = json['levels'];
    List<DataPointLevel>? levels;
    if (jsonLevels != null && jsonLevels.isNotEmpty) {
      levels = jsonLevels
          .map<DataPointLevel>((e) => DataPointLevel(
              date: DateTime.tryParse(e['timestamp']?.toString() ?? "") ??
                  DateTime.now(),
              levelPercent: e['level_percent'],
              levelT: e['level_t'],
              levelL: e['level_liter'],
              temperature: e['temperature'],
              missingWeight: e['missingWeight']))
          .toList();
    }

    // Parse Metadata : InfosToCalibrate and Problem Cause
    final List<InfoToCalibrate> infosToCalibrate = [];
    DeviceOperationType? deviceOperationType;
    int? siloLegs;
    DeviceBatteryStatus batteryStatus = DeviceBatteryStatus.unknown;
    DeviceWarningCause? warningCause;
    Map<String, dynamic>? metadata = json['metadata'];
    if (metadata != null) {
      String? infoToCalibrate = metadata['info_to_calibrate'];
      if (infoToCalibrate != null) {
        if (infoToCalibrate == "missing_level_delivery") {
          infosToCalibrate
              .addAll([InfoToCalibrate.delivery, InfoToCalibrate.level]);
        } else if (infoToCalibrate == "missing_level") {
          infosToCalibrate.add(InfoToCalibrate.level);
        } else if (infoToCalibrate == "missing_delivery") {
          infosToCalibrate.add(InfoToCalibrate.delivery);
        }
      }
      String? operationSuggestedString = metadata['operation_suggested'];
      if (operationSuggestedString != null &&
          operationSuggestedString.isNotEmpty) {
        deviceOperationType =
            DeviceOperationTypeFactory.from(operationSuggestedString);
      }
      String? batteryStatusString = metadata['battery_status'];
      if (batteryStatusString != null && batteryStatusString.isNotEmpty) {
        batteryStatus = DeviceBatteryStatusFactory.from(batteryStatusString);
      }
      String? warningCauseString = metadata['warning_cause'];
      if (warningCauseString != null && warningCauseString.isNotEmpty) {
        warningCause = DeviceWarningCauseFactory.from(warningCauseString);
      }
      siloLegs = metadata['silo_legs'];
    }

    // Parse devices combined
    List<Device> devicesCombined = [];
    List<dynamic>? jsonDevicesCombined = json['devices_combined'];
    if (jsonDevicesCombined != null && jsonDevicesCombined.isNotEmpty) {
      devicesCombined = jsonDevicesCombined
          .map<Device>((e) => Device.fromDashboardJson(e))
          .toList();
    }

    // Parse Group Poi
    Group? groupPoi;
    if (json['group_poi'] != null) {
      final groupPoiJson = json['group_poi'];
      if (groupPoiJson["group_poi_id"] != null &&
          groupPoiJson["group_poi_name"] != null) {
        groupPoi = Group(
            id: groupPoiJson["group_poi_id"],
            name: groupPoiJson["group_poi_name"]);
      }
    }

    IBCModel? ibcModel;
    if (json['model'] != null) {
      ibcModel = IBCModel(name: json['model']);
    }

    return Device(
        id: json['device_id'],
        name: json['device_name'],
        groupPoi: groupPoi,
        siloLegs: siloLegs,
        reference: json['device_reference'],
        status: _parseStatus(json['status']),
        position: position,
        capacity: json['capa_max'],
        deviceContent: json['deviceContent'] != null
            ? DeviceContent(
                id: json['deviceContent'], name: json['deviceContent'])
            : null,
        deviceContentType: json['deviceContentType'] != null
            ? DeviceContentType(
                id: json['deviceContentType'], name: json['deviceContentType'])
            : null,
        remainingDays: json['remaining_days'],
        installDate:
            DateTime.tryParse(json['device_install_date']?.toString() ?? ""),
        lastDeliveryDate:
            DateTime.tryParse(json['device_last_delivery']?.toString() ?? ""),
        nextDeliveryDate:
            DateTime.tryParse(json['next_delivery']?.toString() ?? ""),
        lastMessageReceivedDate:
            DateTime.tryParse(json['lastMessageReceived']?.toString() ?? ""),
        lastCalibrationDate: lastCalibrationDate,
        lastTotalOrderInTons: json['device_last_order_total'],
        levels: levels,
        infosToCalibrate: infosToCalibrate,
        operationSuggested: deviceOperationType,
        isCombined: json['is_combined'],
        isFake: json['is_fake'],
        max15Days: json['max_15days'],
        mean15Days: json['mean_15days'],
        devicesCombined: devicesCombined,
        hasParentCombinedDevice: json['has_parent_combined_device'],
        batteryStatus: batteryStatus,
        warningCause: warningCause,
        ibcModel: ibcModel,
        serialNumber: json['serial_number'],
        isTank: json['is_tank'],
        isSilo: json['is_silo'],
        deviceFabricationDate: DateTime.tryParse(
            json['device_fabrication_date']?.toString() ?? ""),
        color: json['color']);
  }

  static DeviceStatus _parseStatus(String? status) {
    if (status != null) {
      if (status == "ok") {
        return DeviceStatus.ok;
      } else if (status == "calibrating") {
        return DeviceStatus.calibrating;
      } else if (status == "problem") {
        return DeviceStatus.problem;
      } else if (status == "error") {
        return DeviceStatus.error;
      } else if (status == "level_problem") {
        return DeviceStatus.levelProblem;
      } else if (status == "calibration_problem") {
        return DeviceStatus.calibrationProblem;
      } else if (status == "pending") {
        return DeviceStatus.pending;
      }
    }
    return DeviceStatus.unknown;
  }

  factory Device.fromDevicesJson(Map<String, dynamic> json) {
    DeviceContent? deviceContent;
    if (json['deviceContent'] != null) {
      Map<String, dynamic> deviceContentJson = json['deviceContent'];
      if (deviceContentJson['id'] != null &&
          deviceContentJson['device_content'] != null) {
        deviceContent = DeviceContent.fromAPIJson(json['deviceContent']);
      }
    } else if (json['device_content'] != null) {
      deviceContent = DeviceContent(
          id: json['device_content'], name: json['device_content']);
    }

    DeviceContentType? deviceContentType;
    if (json['deviceContentType'] != null) {
      Map<String, dynamic> deviceContentTypeJson = json['deviceContentType'];
      if (deviceContentTypeJson['id'] != null &&
          deviceContentTypeJson['device_content_type'] != null) {
        deviceContentType = DeviceContentType.fromJson(deviceContentTypeJson);
      }
    } else if (json['device_content_type'] != null) {
      deviceContentType = DeviceContentType(
          id: json['device_content_type'], name: json['device_content_type']);
    }

    // Parse Metadata : InfosToCalibrate and Problem Cause
    final List<InfoToCalibrate> infosToCalibrate = [];
    DeviceOperationType? deviceOperationType;
    int? siloLegs;
    DeviceBatteryStatus batteryStatus = DeviceBatteryStatus.unknown;
    DeviceWarningCause? warningCause;
    Map<String, dynamic>? metadata = json['metadata'];
    if (metadata != null) {
      String? infoToCalibrate = metadata['info_to_calibrate'];
      if (infoToCalibrate != null) {
        if (infoToCalibrate == "missing_level_delivery") {
          infosToCalibrate
              .addAll([InfoToCalibrate.delivery, InfoToCalibrate.level]);
        } else if (infoToCalibrate == "missing_level") {
          infosToCalibrate.add(InfoToCalibrate.level);
        } else if (infoToCalibrate == "missing_delivery") {
          infosToCalibrate.add(InfoToCalibrate.delivery);
        }
      }
      String? operationSuggestedString = metadata['operation_suggested'];
      if (operationSuggestedString != null &&
          operationSuggestedString.isNotEmpty) {
        deviceOperationType =
            DeviceOperationTypeFactory.from(operationSuggestedString);
      }
      String? batteryStatusString = metadata['battery_status'];
      if (batteryStatusString != null && batteryStatusString.isNotEmpty) {
        batteryStatus = DeviceBatteryStatusFactory.from(batteryStatusString);
      }
      String? warningCauseString = metadata['warning_cause'];
      if (warningCauseString != null && warningCauseString.isNotEmpty) {
        warningCause = DeviceWarningCauseFactory.from(warningCauseString);
      }
      siloLegs = metadata['silo_legs'];
    }

    // Parse Group Poi
    Group? groupPoi;
    if (json["farm_id"] != null && json["farm_name"] != null) {
      groupPoi = Group(id: json["farm_id"], name: json["farm_name"]);
    }

    IBCModel? ibcModel;
    if (json['model'] != null) {
      ibcModel = IBCModel(name: json['model']);
    }

    return Device(
        id: json['device_id'],
        name: json['device_name'],
        groupPoi: groupPoi,
        reference: json['device_reference'],
        status: _parseStatus(json['status']),
        infosToCalibrate: infosToCalibrate,
        batteryStatus: batteryStatus,
        warningCause: warningCause,
        operationSuggested: deviceOperationType,
        installDate:
            DateTime.tryParse(json['device_install_date']?.toString() ?? ""),
        capacity: json['device_capacity'],
        fixedLat: json['device_fixed_lat']?.toDouble(),
        fixedLong: json['device_fixed_lng']?.toDouble(),
        lastTotalOrderInTons: json['device_last_order_total'],
        groupIds: (json['group_ids'] as List<dynamic>?)
            ?.map((e) => (e as String))
            .toList(),
        siloLegs: siloLegs,
        deviceContent: deviceContent,
        deviceContentType: deviceContentType,
        isCombined: json['is_combined'],
        isFake: json['is_fake'],
        ibcModel: ibcModel,
        serialNumber: json['serial_number'],
        deviceFabricationDate: DateTime.tryParse(
            json['device_fabrication_date']?.toString() ?? ""),
        isTank: json['is_tank'],
        color: json['color']);
  }

  @override
  bool operator ==(Object other) =>
      other is Device && other.runtimeType == runtimeType && other.id == id;

  @override
  int get hashCode => id.hashCode;

  @override
  int compareTo(other) {
    final groupPoiUnwrap = groupPoi;
    final otherGroupPoiUnwrap = other.groupPoi;
    if (groupPoiUnwrap != null && otherGroupPoiUnwrap != null) {
      int groupComp = groupPoiUnwrap.name.compareTo(otherGroupPoiUnwrap.name);
      if (groupComp == 0) {
        return name.compareTo(other.name);
      }
      return groupComp;
    } else {
      return name.compareTo(other.name);
    }
  }

  bool get isProblem {
    return (status != null &&
        (status == DeviceStatus.problem ||
            status == DeviceStatus.error ||
            status == DeviceStatus.levelProblem ||
            status == DeviceStatus.calibrationProblem ||
            status == DeviceStatus.unknown));
  }
}
