enum DeviceBatteryStatus { empty, low, medium, high, unknown }

extension DeviceBatteryStatusFactory on DeviceBatteryStatus {
  static DeviceBatteryStatus from(String value) {
    switch (value) {
      case 'empty':
        return DeviceBatteryStatus.empty;
      case 'low':
        return DeviceBatteryStatus.low;
      case 'medium':
        return DeviceBatteryStatus.medium;
      case 'high':
        return DeviceBatteryStatus.high;
      default:
        return DeviceBatteryStatus.unknown;
    }
  }
}
