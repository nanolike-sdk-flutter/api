class JWTToken {
  final String accessToken;
  final String refreshToken;

  JWTToken(
    this.accessToken,
    this.refreshToken,
  );

  factory JWTToken.fromJson(Map<String, dynamic> json) {
    final accessToken = json['access'] as String;
    final refreshToken = json['refresh'] as String;

    return JWTToken(
      accessToken,
      refreshToken,
    );
  }

  Map<String, dynamic> toJson() => {
        'access': accessToken,
        'refresh': refreshToken,
      };
}
