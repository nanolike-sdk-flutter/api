class Role {
  final String id;
  final String? name;
  final String? description;
  final bool isNanolike;
  final bool isGlobal;
  final bool isGroupRoleDelegated;
  final num? hierarchy;
  final Map<String, dynamic>? authorization;
  final String? displayName;
  final String? dedicatedToProduct;

  Role(
      {required this.id,
      this.name,
      this.description,
      this.isGlobal = false,
      this.isNanolike = false,
      this.isGroupRoleDelegated = false,
      this.hierarchy,
      this.dedicatedToProduct,
      this.authorization,
      this.displayName});

  factory Role.fromWorkspaceJson(Map<String, dynamic> json) {
    return Role(
        id: json['workspace_role_id'], name: json['workspace_role_name']);
  }

  factory Role.fromMeJson(Map<String, dynamic> json) {
    return Role(
      id: json['_pivot_workspace_role'],
      name: json['_pivot_workspace_role'],
    );
  }

  factory Role.fromGroupJson(Map<String, dynamic> json) {
    return Role(id: json['group_role_id'], name: json['group_role_name']);
  }

  bool get needGroupAccess => isGlobal == false || isGroupRoleDelegated == true;

  factory Role.fromJson(Map<String, dynamic> json) {
    final num? isNanolikeNum = json['is_nanolike'];
    final num? isGlobalNum = json['global'];
    final num? isGroupRoleDelegatedNum = json['group_role_delegation'];

    return Role(
        id: json['idRole'],
        name: json['name'],
        displayName: json['display_name'],
        description: json['description'],
        isGlobal: (isGlobalNum != null && isGlobalNum == 1) ? true : false,
        isGroupRoleDelegated:
            (isGroupRoleDelegatedNum != null && isGroupRoleDelegatedNum == 1)
                ? true
                : false,
        hierarchy: json['hierarchy'],
        dedicatedToProduct: json['dedicated_to_product'],
        isNanolike:
            (isNanolikeNum != null && isNanolikeNum == 1) ? true : false,
        authorization: json['authorisation']);
  }
}
