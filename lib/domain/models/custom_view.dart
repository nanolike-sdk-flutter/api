import 'package:flutter_nanolike_sdk/domain/models/filters.dart';

class CustomView {
  final String id;
  final String name;
  final Filters filters;

  CustomView({required this.id, required this.name, required this.filters});

  factory CustomView.fromJson(Map<String, dynamic> json) {
    return CustomView(
        id: json['idCustomView'],
        name: json['name'],
        filters: Filters.fromJsonFromCustomView(json['filters']));
  }

  Map<String, dynamic> toJson() {
    return {'id': id, 'name': name, 'filters': filters.toJsonFromCustomView()};
  }
}
