enum CalibrationType {
  delivery,
  level,
}

enum CalibrationSource {
  delivery,
  manual,
}

class Calibration {
  final String id;
  final String deviceReference;
  final String? deviceName;
  final String? groupName;
  final DateTime? calibrationDate;
  final num? quantity;
  final num? capacity;
  final CalibrationType? type;
  final CalibrationSource? source;

  Calibration(
      {required this.id,
      required this.deviceReference,
      this.deviceName,
      this.groupName,
      this.calibrationDate,
      this.quantity,
      this.type,
      this.source,
      this.capacity});

  factory Calibration.fromJson(Map<String, dynamic> json) {
    return Calibration(
      id: json['idCalibration'],
      deviceReference: json['idDevice'],
      type: json['type'] == 'delivery'
          ? CalibrationType.delivery
          : CalibrationType.level,
      source: json['source'] == 'delivery'
          ? CalibrationSource.delivery
          : CalibrationSource.manual,
      deviceName: json['device_name'],
      groupName: json['poi_name'],
      calibrationDate: json['calibration_date'] != null
          ? DateTime.parse(json['calibration_date'])
          : null,
      quantity: json['quantity'],
      capacity: json['capa_max'],
    );
  }
}
