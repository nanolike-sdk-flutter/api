import 'dart:core';

class GraphData {
  final String deviceId;
  final String deviceName;
  final String deviceReference;
  final String? groupName;
  final String? dataType;
  final String? type;
  final List<IGraphDataPoint>? dataPoints;

  GraphData(
      {required this.deviceId,
      required this.deviceName,
      required this.deviceReference,
      this.groupName,
      this.dataType,
      this.type,
      this.dataPoints});

  factory GraphData.fromJson(Map<String, dynamic> json) {
    List<IGraphDataPoint>? dataPoints;
    if (json['data_points'] != null) {
      dataPoints = (json['data_points'] as List<dynamic>).map((e) {
        if (json['data_type'] == 'level') {
          return DataPointLevel.fromJson(e as Map<String, dynamic>);
        } else {
          return DataPointValue.fromJson(e as Map<String, dynamic>);
        }
      }).toList();
    }

    return GraphData(
      deviceId: json['device_id'],
      deviceName: json['device_name'],
      deviceReference: json['device_reference'],
      groupName: json['farm_name'],
      dataType: json['data_type'],
      type: json['type'],
      dataPoints: dataPoints,
    );
  }
}

abstract class IGraphDataPoint {
  final DateTime date;

  IGraphDataPoint({required this.date});
}

class DataPointValue extends IGraphDataPoint {
  final dynamic value;

  DataPointValue({required super.date, required this.value});

  factory DataPointValue.fromJson(Map<String, dynamic> json) {
    return DataPointValue(
      date: DateTime.tryParse(json['timestamp']?.toString() ?? "") ??
          DateTime.now(),
      value: json['value'],
    );
  }
}

class DataPointLevel extends IGraphDataPoint {
  final num? levelPercent;
  final num? levelT;
  final num? levelL;
  final num? temperature;
  final num? missingWeight;

  DataPointLevel(
      {required super.date,
      this.levelPercent,
      this.levelT,
      this.levelL,
      this.temperature,
      this.missingWeight});

  factory DataPointLevel.fromJson(Map<String, dynamic> json) {
    return DataPointLevel(
        date: DateTime.tryParse(json['timestamp']?.toString() ?? "") ??
            DateTime.now(),
        levelPercent: json['level_percent'],
        levelT: json['level_t'],
        levelL: json['level_liter'],
        temperature: json['temperature'],
        missingWeight: json['missingWeight']);
  }
}
