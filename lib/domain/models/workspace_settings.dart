// ignore_for_file: sdk_version_since

import 'package:flutter_nanolike_sdk/domain/models/order.dart';
import 'package:intl/intl.dart';
import 'package:flutter_nanolike_sdk/helpers/extensions.dart';

enum UseCase { tank, bin, silo }

enum BinconnectV2 { partial, total }

class WorkspaceSettings {
  final String? workspaceName;
  final num? orderSuggestionThreshold;
  final num? orderThreshold;
  final num? maxArticlesNb;
  final BinconnectV2? binconnectV2;
  final bool? showDrugs;
  final bool? showSupplements;
  final List<DateTime>? bankHolidays;
  final List<int>? enabledWeekdays;
  final List<TimeSlot>? deliveryTimes;
  final num? minHoursBeforeOrder;
  final String? supportPhone;
  final String? supportEmail;
  final bool? deliveryDateRange;
  final UseCase? useCase;
  final bool? disableOrders;
  final bool? disableOrderEdition;
  final List<String>? deliveryRounds;
  final String? supportWhatsApp;
  final String? fillingUnit;
  final bool? filterContentByGroup;
  final String? logoApp;

  WorkspaceSettings(
      {this.workspaceName,
      this.orderSuggestionThreshold,
      this.orderThreshold,
      this.binconnectV2,
      this.maxArticlesNb,
      this.showDrugs,
      this.showSupplements,
      this.bankHolidays,
      this.enabledWeekdays,
      this.deliveryTimes,
      this.minHoursBeforeOrder,
      this.supportPhone,
      this.supportEmail,
      this.deliveryDateRange,
      this.useCase,
      this.disableOrders,
      this.disableOrderEdition,
      this.deliveryRounds,
      this.supportWhatsApp,
      this.fillingUnit,
      this.filterContentByGroup,
      this.logoApp});

  factory WorkspaceSettings.fromJson(Map<String, dynamic> json) {
    final List<dynamic> results = json['results'];
    final settings = {for (var e in results) e['key']: e['value']};

    // Parse delivery times
    final List<TimeSlot> timeSlots = [];
    final String? deliveryTimesConstants = settings['deliveryTimesConstants'];
    if (deliveryTimesConstants != null && deliveryTimesConstants.isNotEmpty) {
      if (deliveryTimesConstants.contains(TimeSlot.morning.requestValue)) {
        timeSlots.add(TimeSlot.morning);
      }
      if (deliveryTimesConstants.contains(TimeSlot.afternoon.requestValue)) {
        timeSlots.add(TimeSlot.afternoon);
      }
    }

    // Parse delivery round
    final List<String> deliveryRounds = [];
    final String? deliveryRoundsString = settings['deliveryRounds'];
    if (deliveryRoundsString != null && deliveryRoundsString.isNotEmpty) {
      deliveryRounds.addAll(deliveryRoundsString.split(','));
    }

    // Parse use case
    UseCase? useCase;
    final String? useCaseSring = settings['useCase'];
    if (useCaseSring != null && useCaseSring.isNotEmpty) {
      if (useCaseSring == 'silo') {
        useCase = UseCase.bin;
      } else if (useCaseSring == 'ibc') {
        useCase = UseCase.tank;
      } else if (useCaseSring == 'silo_industry') {
        useCase = UseCase.silo;
      }
    }

    // Parse settingv2 for binconnect
    BinconnectV2? settingBinconnectV2;
    final String? settingBinconnectV2String = settings['settingV2'];
    if (settingBinconnectV2String != null &&
        settingBinconnectV2String.isNotEmpty) {
      if (settingBinconnectV2String == 'partial') {
        settingBinconnectV2 = BinconnectV2.partial;
      } else if (settingBinconnectV2String == 'total') {
        settingBinconnectV2 = BinconnectV2.total;
      }
    }

    // Parse bank holidays
    final List<DateTime> bankHolidays = [];
    final String? orderDatesDisabledString = settings['orderDatesDisabled'];

    if (orderDatesDisabledString != null &&
        orderDatesDisabledString.isNotEmpty) {
      orderDatesDisabledString.split(',').forEach((e) {
        if (e.isValidBankHoliday()) {
          bankHolidays.add(DateFormat('M/d/yyyy').parse(e));
        }
      });
    }

    // Parse enable weekday
    final List<int> enabledWeekdays = [];
    final String? orderWeekdaysEnabledString = settings['orderWeekdaysEnabled'];
    if (orderWeekdaysEnabledString != null &&
        orderWeekdaysEnabledString.isNotEmpty) {
      final Map<String, int> weekdayMap = {
        'monday': DateTime.monday,
        'tuesday': DateTime.tuesday,
        'wednesday': DateTime.wednesday,
        'thursday': DateTime.thursday,
        'friday': DateTime.friday,
        'saturday': DateTime.saturday,
        'sunday': DateTime.sunday,
      };
      List<String> orderWeekdaysEnabledList =
          orderWeekdaysEnabledString.split(',');
      enabledWeekdays.addAll(
          orderWeekdaysEnabledList.map((day) => weekdayMap[day] ?? 0).toList());
      enabledWeekdays.removeWhere((day) => day == 0);
    }

    // Parse min time friday
    DateTime now = DateTime.now();
    if (now.weekday == DateTime.friday ||
        now.weekday == DateTime.saturday ||
        now.weekday == DateTime.sunday) {
      final String? minTimeFriday = settings['minTimeFriday'];
      if (minTimeFriday != null && minTimeFriday.isNotEmpty) {
        final minTimeFridaySplit = minTimeFriday.split(':');
        if (minTimeFridaySplit.length == 2) {
          int? hours = int.tryParse(minTimeFridaySplit[0]);
          int? minutes = int.tryParse(minTimeFridaySplit[1]);

          if (hours != null && minutes != null) {
            final limit =
                DateTime(now.year, now.month, now.day, hours, minutes);
            if (now.isAfter(limit)) {
              int nbDaysToAdd = 1;
              if (now.weekday == DateTime.saturday) {
                nbDaysToAdd += 1;
              } else if (now.weekday == DateTime.friday) {
                nbDaysToAdd += 2;
              }
              bankHolidays.add(now.add(Duration(days: nbDaysToAdd)));
            }
          }
        }
      }
    }

    return WorkspaceSettings(
        workspaceName: settings['workspace_name'],
        orderThreshold: (settings['orderThreshold'] != null)
            ? int.tryParse(settings['orderThreshold'])
            : null,
        orderSuggestionThreshold:
            (settings['order_suggestion_threshold_tons'] != null)
                ? int.tryParse(settings['order_suggestion_threshold_tons'])
                : null,
        maxArticlesNb: (settings['maxArticlesNb'] != null)
            ? int.tryParse(settings['maxArticlesNb'])
            : null,
        showDrugs: settings['showDrugs'] != null
            ? bool.tryParse(settings['showDrugs'], caseSensitive: false)
            : null,
        showSupplements: settings['showSupplements'] != null
            ? bool.tryParse(settings['showSupplements'], caseSensitive: false)
            : null,
        minHoursBeforeOrder: (settings['minHoursBeforeOrder'] != null)
            ? int.tryParse(settings['minHoursBeforeOrder'])
            : null,
        supportPhone: settings['support_phone'],
        supportEmail: settings['support_email'],
        deliveryDateRange: settings['deliveryDateRange'] != null
            ? bool.tryParse(settings['deliveryDateRange'], caseSensitive: false)
            : null,
        useCase: useCase,
        disableOrders: settings['disableOrders'] != null
            ? bool.tryParse(settings['disableOrders'], caseSensitive: false)
            : null,
        disableOrderEdition: settings['disableOrderEdition'] != null
            ? bool.tryParse(settings['disableOrderEdition'],
                caseSensitive: false)
            : null,
        filterContentByGroup: settings['filterContentByGroup'] != null
            ? bool.tryParse(settings['filterContentByGroup'],
                caseSensitive: false)
            : null,
        binconnectV2: settingBinconnectV2,
        deliveryTimes: timeSlots.isEmpty ? null : timeSlots,
        bankHolidays: bankHolidays.isEmpty ? null : bankHolidays,
        enabledWeekdays: enabledWeekdays.isEmpty ? null : enabledWeekdays,
        deliveryRounds: deliveryRounds.isEmpty ? null : deliveryRounds,
        supportWhatsApp: settings['whatsapp_link'],
        fillingUnit: settings['fillingUnit'],
        logoApp: settings['logoApp']);
  }
}
