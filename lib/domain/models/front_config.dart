class FrontConfig {
  final String? minVersion;

  FrontConfig({this.minVersion});

  factory FrontConfig.fromJson(Map<String, dynamic> json) {
    return FrontConfig(minVersion: json['min_version']);
  }
}
