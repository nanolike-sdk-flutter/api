enum ReportStatus { ok, problem }

class Report {
  final ReportStatus status;
  final DateTime? timestamp;

  Report({required this.status, this.timestamp});

  factory Report.fromJson(Map<String, dynamic> json) {
    final List<dynamic> data = json['data'];
    final Map<String, dynamic> reportData = data.first;
    return Report(
        status: reportData['result'] == 'ok'
            ? ReportStatus.ok
            : ReportStatus.problem,
        timestamp: reportData['timestamp'] != null
            ? DateTime.parse(reportData['timestamp'])
            : null);
  }
}
