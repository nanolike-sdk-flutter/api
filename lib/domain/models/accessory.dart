import 'dart:core';

class Accessory {
  final int id;
  final String sigfoxId;
  final AccessoryType type;

  Accessory(this.id, this.sigfoxId, this.type);

  factory Accessory.fromJson(Map<String, dynamic> json) {
    return Accessory(json['id'], json['accessory_id'],
        AccessoryTypeFactory.from(json['type']));
  }
}

enum AccessoryType {
  baseStation,
  repeater, // not used anymore
  unknown // not used anymore
}

extension AccessoryTypeFactory on AccessoryType {
  static AccessoryType from(String string) {
    if (string == AccessoryType.baseStation.serverString()) {
      return AccessoryType.baseStation;
    } else if (string == AccessoryType.repeater.serverString()) {
      return AccessoryType.repeater;
    } else {
      return AccessoryType.unknown;
    }
  }

  String serverString() {
    switch (this) {
      case AccessoryType.baseStation:
        return "BASE_STATION";
      case AccessoryType.repeater:
        return "REPEATER";
      case AccessoryType.unknown:
        return "UNKNOWN";
    }
  }
}
