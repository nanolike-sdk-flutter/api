enum DeviceOperationType {
  // SILO
  newElec,
  newSensor,
  newDeviceFull,
  move,
  reinstallSameLeg,
  newBattery,
  shim,
  connectSensor,
  outOfData,
  trappedInIce,
  other,
  // IBC
  newWeighingUnit,
  unknown
}

extension DeviceOperationTypeFactory on DeviceOperationType {
  static final Map<String, DeviceOperationType> _stringToEnum = {
    "new_elec": DeviceOperationType.newElec,
    "new_sensor": DeviceOperationType.newSensor,
    "new_device_full": DeviceOperationType.newDeviceFull,
    "move": DeviceOperationType.move,
    "reinstall_same_leg": DeviceOperationType.reinstallSameLeg,
    "new_battery": DeviceOperationType.newBattery,
    "shim": DeviceOperationType.shim,
    "new_weighing_unit": DeviceOperationType.newWeighingUnit,
    "connect_sensor": DeviceOperationType.connectSensor,
    "out_of_data": DeviceOperationType.outOfData,
    "trapped_in_ice": DeviceOperationType.trappedInIce,
    "other": DeviceOperationType.other,
    "unknown": DeviceOperationType.unknown,
  };

  static DeviceOperationType from(String string) =>
      _stringToEnum[string] ?? DeviceOperationType.unknown;

  String get serverString =>
      _stringToEnum.entries.firstWhere((entry) => entry.value == this).key;
}
