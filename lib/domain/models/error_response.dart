class ErrorResponse {
  final String? code;
  final String? message;

  ErrorResponse({this.code, this.message});

  factory ErrorResponse.fromJson(Map<String, dynamic>? json) {
    return ErrorResponse(code: json?['code'], message: json?['message']);
  }
}
