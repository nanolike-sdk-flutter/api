import 'antenna.dart';

class ConnectivityMessage {
  final int? number;
  final num? adc;
  final List<Antenna> antennas;
  final List<Antenna> repeaters;
  final DateTime? timeData;
  final bool? isDeviceOk;
  final ConnectivityStatus status;

  ConnectivityMessage(
      {this.number,
      this.adc,
      required this.antennas,
      required this.repeaters,
      this.timeData,
      this.isDeviceOk,
      required this.status});

  factory ConnectivityMessage.fromJson(Map<String, dynamic> json) {
    return ConnectivityMessage(
        number: json['seqNumber'],
        adc: json['adc'],
        antennas: json['antennas']
                ?.map<Antenna>((e) => Antenna.fromJson(e))
                .toList() ??
            [],
        repeaters: json['repeaters']
                ?.map<Antenna>((e) => Antenna.fromJson(e))
                .toList() ??
            [],
        timeData: DateTime.tryParse(json['timeData']?.toString() ?? ""),
        isDeviceOk: json['isDeviceOk'],
        status: _parseStatus(json['status']));
  }

  static ConnectivityStatus _parseStatus(String? status) {
    if (status != null) {
      if (status == "ok") {
        return ConnectivityStatus.ok;
      } else if (status == "no_sensor") {
        return ConnectivityStatus.noSensor;
      } else if (status == "adc_out_of_range") {
        return ConnectivityStatus.adcOutOfRange;
      }
    }
    return ConnectivityStatus.unknown;
  }
}

enum ConnectivityStatus { ok, noSensor, adcOutOfRange, unknown }
