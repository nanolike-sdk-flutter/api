import 'package:flutter_nanolike_sdk/domain/models/group.dart';

class GroupMembership {
  final String id;
  final Group group;

  GroupMembership({required this.id, required this.group});

  factory GroupMembership.fromJson(Map<String, dynamic> json) {
    return GroupMembership(
        id: json['user_group_membership_id'],
        group: Group.fromAPIJson(json['group']));
  }
}
