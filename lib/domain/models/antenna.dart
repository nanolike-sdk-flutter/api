class Antenna {
  final String bsId;
  final num? nbRep;
  final num? rssi;

  Antenna({required this.bsId, this.nbRep, this.rssi});

  factory Antenna.fromJson(Map<String, dynamic> json) {
    return Antenna(
        bsId: json['bsId'], nbRep: json['nbRep'], rssi: json['rssi']);
  }
}
