import 'package:flutter_nanolike_sdk/domain/models/filters.dart';
import 'package:flutter_nanolike_sdk/domain/models/session.dart';
import 'package:flutter_nanolike_sdk/domain/models/token.dart';

abstract class IStorageProvider {
  // ISession
  Future saveSession(Session session);
  Future<Session?> getSession();
  Future removeSession();
  // JWTToken
  Future saveJWTToken(JWTToken token);
  Future<JWTToken?> getJWTToken();
  // Clear storage
  Future removeAll();
  // Filters
  Future saveFilters({Filters? filters});
  Future<Filters?> getFilters();
}
