import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_nanolike_sdk/domain/models/locale_extension.dart';

void main() {
  group('LocaleExtensions', () {
    test('flagUrl should return the correct URL for each locale', () {
      expect(const Locale('zh_CH').flagUrl, 'https://flagcdn.com/24x18/cn.png');
      expect(const Locale('sv').flagUrl, 'https://flagcdn.com/24x18/se.png');
      expect(const Locale('zh_TW').flagUrl, 'https://flagcdn.com/24x18/tw.png');
      expect(const Locale('ja').flagUrl, 'https://flagcdn.com/24x18/jp.png');
      expect(const Locale('vi').flagUrl, 'https://flagcdn.com/24x18/vn.png');
      expect(const Locale('en').flagUrl, 'https://flagcdn.com/24x18/gb.png');
      expect(const Locale('en_US').flagUrl, 'https://flagcdn.com/24x18/us.png');
      expect(const Locale('da').flagUrl, 'https://flagcdn.com/24x18/dk.png');
      expect(const Locale('en_CA').flagUrl, 'https://flagcdn.com/24x18/ca.png');
      expect(const Locale('fr_CA').flagUrl, 'https://flagcdn.com/24x18/ca.png');
    });

    test('languageName should return the correct name for each locale', () {
      expect(const Locale('da').languageName, 'Danske');
      expect(const Locale('de').languageName, 'Deutsch');
      expect(const Locale('en_CA').languageName, 'English (Canada)');
      expect(const Locale('en_US').languageName, 'English (US)');
      expect(const Locale('en').languageName, 'English');
      expect(const Locale('es').languageName, 'Español');
      expect(const Locale('fr_US').languageName, 'Français');
      expect(const Locale('fr').languageName, 'Français');
      expect(const Locale('fr_CA').languageName, 'Français (Canada)');
      expect(const Locale('it').languageName, 'Italiano');
      expect(const Locale('pl').languageName, 'Polski');
      expect(const Locale('pt').languageName, 'Português');
      expect(const Locale('sv').languageName, 'Svenska');
      expect(const Locale('vi').languageName, 'Tiếng Việt');
      expect(const Locale('th').languageName, 'ไทย');
      expect(const Locale('ru').languageName, 'Русский');
      expect(const Locale('zh_CH').languageName, '简体中文');
      expect(const Locale('zh_TW').languageName, '簡體中文（台灣）');
      expect(const Locale('ja').languageName, '日本語');
    });
  });

  group('LocaleUtils', () {
    test('fromString should return the correct Locale object', () {
      expect(LocaleUtils.fromString('en'), const Locale('en'));
      expect(LocaleUtils.fromString('en_US'), const Locale('en', 'US'));
      expect(LocaleUtils.fromString('en_CA'), const Locale('en', 'CA'));
      expect(LocaleUtils.fromString('fr_CA'), const Locale('fr', 'CA'));
      expect(LocaleUtils.fromString('zh_CH'), const Locale('zh', 'CH'));
      expect(LocaleUtils.fromString('zh_TW'), const Locale('zh', 'TW'));
      expect(LocaleUtils.fromString('en-US'), const Locale('en', 'US'));
      expect(LocaleUtils.fromString('en-CA'), const Locale('en', 'CA'));
      expect(LocaleUtils.fromString('fr-CA'), const Locale('fr', 'CA'));
      expect(LocaleUtils.fromString('zh-CH'), const Locale('zh', 'CH'));
      expect(LocaleUtils.fromString('zh-TW'), const Locale('zh', 'TW'));
      expect(LocaleUtils.fromString('ja'), const Locale('ja'));
    });
  });
}
