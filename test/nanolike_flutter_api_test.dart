import 'package:flutter_nanolike_sdk/helpers/extensions.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('isValidBankHoliday', () {
    expect("1/1/2024".isValidBankHoliday(), true);
    expect("2/1/2024".isValidBankHoliday(), true);
    expect("3/1/2024".isValidBankHoliday(), true);
    expect("4/1/2024".isValidBankHoliday(), true);
    expect("5/1/2024".isValidBankHoliday(), true);
    expect("6/1/2024".isValidBankHoliday(), true);
    expect("7/1/2024".isValidBankHoliday(), true);
    expect("8/1/2024".isValidBankHoliday(), true);
    expect("9/1/2024".isValidBankHoliday(), true);
    expect("10/1/2024".isValidBankHoliday(), true);
    expect("11/1/2024".isValidBankHoliday(), true);
    expect("12/1/2024".isValidBankHoliday(), true);
    expect("12/31/2024".isValidBankHoliday(), true);

    expect("0/1".isValidBankHoliday(), false);
    expect("0/1/24".isValidBankHoliday(), false);
    expect("1/1".isValidBankHoliday(), false);
    expect("1/1/24".isValidBankHoliday(), false);
    expect("1/1/242".isValidBankHoliday(), false);
    expect("1/1/24244".isValidBankHoliday(), false);
    expect("0/1/2024".isValidBankHoliday(), false);
    expect("13/1/2024".isValidBankHoliday(), false);
    expect("1/0/2024".isValidBankHoliday(), false);
    expect("1/32/2024".isValidBankHoliday(), false);
  });

  test('isValidEmail', () {
    expect("email@email.com".isValidEmail(), true);
    expect("email@email-email.com".isValidEmail(), true);
    expect("email-email@email.com".isValidEmail(), true);
    expect("email.email@email.com".isValidEmail(), true);
    expect("email+email@email.com".isValidEmail(), true);
    expect("email@e.e".isValidEmail(), true);

    expect("email".isValidEmail(), false);
    expect("email@".isValidEmail(), false);
    expect("email@email@email.com".isValidEmail(), false);
    expect("".isValidEmail(), false);
    expect(" ".isValidEmail(), false);
    expect("email@email".isValidEmail(), false);
    expect("email@.com".isValidEmail(), false);
  });

  test('tryParseDouble', () {
    expect("hello".tryParseDouble(), null);
    expect("1".tryParseDouble(), 1);
    expect("0.0002".tryParseDouble(), 0.0002);
    expect("0,0002".tryParseDouble(), 0.0002);
  });

  test('firstOrNull', () {
    expect([].firstOrNull, null);
    expect(["1"].firstOrNull, "1");
    expect(["1", "2"].firstOrNull, "1");
    expect([].firstWhereOrNull((element) => element == "2"), null);
    expect(["1", "2"].firstWhereOrNull((element) => element == "2"), "2");
    expect([null, "1", "2"].firstWhereOrNull((element) => element == "2"), "2");
    expect(["1", "2", "3", "2"].firstWhereOrNull((element) => element == "2"),
        "2");
    expect(["1", "2"].firstWhereOrNull((element) => element == "3"), null);
  });
}
